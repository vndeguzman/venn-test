# Venn Frontend Test #

Create a website using either ReactJS, Angular, or VueJS (VueJS is preferred) that will consume JSONPlaceholder's REST API (https://jsonplaceholder.typicode.com/). Please use its Users and Posts endpoints.


### Required Features ###
 - CRUD of Users
    - POST /users/
    - GET /users/<id>/
    - PATCH /users/<id>/
    - DELETE /users/<id>/

- CRUD of Posts
    - POST /posts/
    - GET /posts/<id>/
    - PATCH /posts/<id>/
    - DELETE /posts/<id>/

- On the upper left hand side, show the listing of  users
    - GET /users/

- On the lower left hand side, show the listing of  posts
    - GET /posts/

- The top search bar on the left hand side should search for users by username
    - GET /users?username=<username>

- The bottom search bar on the left hand side should search for posts by title
    - GET /posts?title=<title>

- When user clicked on one of the users on the upper left hand side, the right panel of the window should list the posts made by the selected user.
    - GET /posts?userId=<id>

- When user clicked on one of the posts on the lower left hand side, the right panel of the window should list the comments under that post.
    - GET 	/comments?postId=1

- For the forms / elements that are not shown in the attached assets / photos, we will let you decide how it will look.

- The search on the right hand side should be removed. No search on that part.


### Plus Points ###
- Convert the Web App to an ElectronJS Desktop App
- Setup ElectronJS Desktop App such that it can do Autoupdate

### Setup ###

- Requires [Node.js](https://nodejs.org/) v10+ and [Yarn](https://yarnpkg.com/).

- Clone this repo and `cd` to the working directory: `$ cd venn-test`

- Install dependencies by running: `$ yarn install`

- Start the server: `$ yarn run dev`


### Thanks! ###